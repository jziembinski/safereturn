//
//  LoginViewController.swift
//  SafeReturn
//
//  Created by Jakub Ziembiński on 07/04/2017.
//  Copyright © 2017 Jakub Ziembiński. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {

    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var repeatPasswordTextField: UITextField!
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var goToRegisterVCButton: UIButton!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var borderBottom3: UIView!
    @IBOutlet weak var registerButton: UIButton!
    @IBOutlet weak var lostPassword: UIButton!
    
    @IBOutlet weak var keyboardViewHeightConstraint: NSLayoutConstraint!
    let apiClient = FirebaseAPI()
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        subscribeToKeyboardNotifications()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        emailTextField.delegate = self

        if title == "Login" {
        
        } else {
            titleLabel.text = "Rejestracja"
            loginButton.isHidden = true
            lostPassword.isHidden = true
            goToRegisterVCButton.isHidden = true
            repeatPasswordTextField.isHidden = false
            borderBottom3.isHidden = false
            registerButton.isHidden = false
        }
        
        let touchGestureRecognizer: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(hideKeyboard))
        self.view.addGestureRecognizer(touchGestureRecognizer)
    }

    func subscribeToKeyboardNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: .UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: .UIKeyboardWillHide, object: nil)
    }

    func unsubscribeFromKeyboardNotifications() {
        NotificationCenter.default.removeObserver(self, name: .UIKeyboardWillShow, object: nil)
        NotificationCenter.default.removeObserver(self, name: .UIKeyboardWillHide, object: nil)
    }

    func keyboardWillShow(_ notification: Notification) {
        if emailTextField.isFirstResponder || passwordTextField.isFirstResponder || repeatPasswordTextField.isFirstResponder {
            if (keyboardViewHeightConstraint.constant == 0) {
                UIView.animate(withDuration: 4, animations: {
                    self.keyboardViewHeightConstraint.constant += self.getKeyboardHeight(notification) / 2
                    self.view.layoutIfNeeded()
                })
            }
        }
    }

    func keyboardWillHide(_ notification: Notification) {
        if emailTextField.isFirstResponder || passwordTextField.isFirstResponder || repeatPasswordTextField.isFirstResponder {
                UIView.animate(withDuration: 4, animations: {
                    self.keyboardViewHeightConstraint.constant -= self.getKeyboardHeight(notification) / 2
                    self.view.layoutIfNeeded()
                })
        }
    }

    func getKeyboardHeight(_ notification: Notification) -> CGFloat {
        let userInfo = notification.userInfo
        let keyboardSize = userInfo![UIKeyboardFrameEndUserInfoKey] as! NSValue
        return keyboardSize.cgRectValue.height
    }
    
    func hideKeyboard() {
        view.endEditing(true)
        keyboardViewHeightConstraint.constant = 0
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    //    if let vc: UITabBarController = segue.destination as? UITabBarController, let apiClient = sender as? FirebaseAPI, segue.identifier == "showTricksTableView" {
    //        for child in vc.viewControllers! {
    //            guard let navigationController = child as? GradientBackgroundNavigationController else  { return }
    //            guard let topViewController = navigationController.topViewController as? ApiClientUser else { return }
    //            topViewController.set(apiClient: apiClient)
    //        }
    //    }
    }

    // MARK: - IBActions

    @IBAction func didTapLoginButton(_ sender: Any) {
        if let email = emailTextField.text, let password = passwordTextField.text {
            apiClient.loginUserWith(email: email, password: password, success: {
                self.performSegue(withIdentifier: "showWalkViewController", sender: self.apiClient)
            }, failure: { (error) in
                print("There was an error with logging: \(String(describing: error))")
            })
        }
    }

    @IBAction func didTapGoToRegisterVCButton(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "loginViewController") as? LoginViewController
        
        guard let loginViewController = vc else {return}
        
        loginViewController.title = "Register"
        present(loginViewController, animated: true)
    }
    
    @IBAction func didTapRegisterButton(_ sender: Any) {
    
    // TODO: Add all input-error related stuff
    
        if let email = emailTextField.text, let password = passwordTextField.text, let repeatedPassword = repeatPasswordTextField.text, password == repeatedPassword  {
            
            apiClient.registerUser(email: email, password: password, success: { _ in
                self.apiClient.loginUserWith(email: email, password: password, success: {
                    DispatchQueue.main.async {
                        self.performSegue(withIdentifier: "showWalkViewController", sender: self.apiClient)
                    }
                }, failure: { (error) in
                    guard let error = error else {return}
                    print(error)
                })
                
            }, failure: { (error) in
                guard let error = error else {return}
                print(error)
            })
        }
    }
}

extension LoginViewController: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
    }
}

//
//  ViewController.swift
//  SafeReturn
//
//  Created by Jakub Ziembiński on 07/04/2017.
//  Copyright © 2017 Jakub Ziembiński. All rights reserved.
//

import UIKit
import MapKit

class WalkViewController: UIViewController, CLLocationManagerDelegate {

    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var panicButton: GeneralButton!
    @IBOutlet weak var tagButton: GeneralButton!
    @IBOutlet weak var checkButton: GeneralButton!
    
    let locationManager: CLLocationManager = CLLocationManager()
    
    var touchBeginTime: Date?
    
    @IBAction func panicButtonTapped(_ sender: Any) {
        self.panicButton.dim()
        let elapsed = Date().timeIntervalSince(touchBeginTime!)
        if elapsed >= 3 {
            callForPolice()
        }
    }
    
    
    @IBAction func panicButtonTouchDown(_ sender: Any) {
        touchBeginTime = Date()
        self.panicButton.highlight(duration: 4, to: 1)
        print("DOWN")
    }
    
    @IBAction func tagButtonTapped(_ sender: Any) {
        self.tagButton.highlight(duration: 0.1, to: 0.75)
    }
    
    
    @IBAction func checkButtonTapped(_ sender: Any) {
        self.checkButton.layer.borderColor = UIColor.checkUsed.cgColor
        self.checkButton.highlight(duration: 0.3, to: 0.9)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setUpButtons()
        
        mapView.showsCompass = false
        locationManager.delegate = self
        
        if CLLocationManager.authorizationStatus() == .authorizedAlways {
            startLocationGathering()
        } else {
            locationManager.requestAlwaysAuthorization()
            if CLLocationManager.authorizationStatus() == .authorizedAlways {
                startLocationGathering()
            }
        }
        
        self.setLocationButton()
    }
    
    private func setUpButtons() {
        panicButton.layer.borderColor = UIColor.panicBorder.cgColor
        tagButton.layer.borderColor = UIColor.tagBorder.cgColor
        checkButton.layer.borderColor = UIColor.checkBorder.cgColor
    }
    
    private func setLocationButton() {
        if let locationButton = MKUserTrackingBarButtonItem(mapView: self.mapView).customView {
            
            self.view.addSubview(locationButton)
            locationButton.translatesAutoresizingMaskIntoConstraints = false
            
            self.view.addConstraint(
                NSLayoutConstraint(
                    item: locationButton,
                    attribute: .trailing,
                    relatedBy: .equal,
                    toItem: self.panicButton,
                    attribute: .trailing,
                    multiplier: 1,
                    constant: 0.0)
            )
            
            self.view.addConstraint(
                NSLayoutConstraint(
                    item: locationButton,
                    attribute: .bottom,
                    relatedBy: .equal,
                    toItem: self.panicButton,
                    attribute: .top,
                    multiplier: 1,
                    constant: -32.0)
            )
        }
    }
    
    func callForPolice() {
        print("Calling for police!")
        let phone = "tel://122";
        if let url: URL = URL(string: phone) {
            UIApplication.shared.open(url, options: [:])
            print("URL OPENED")
        }
    }
    
    func startLocationGathering() {
        locationManager.startUpdatingLocation()
        mapView.showsUserLocation = true
        guard let location = locationManager.location else {
            print("WalkViewController: startLocationGathering - couldn't get user initial location")
            return
        }
        centerMapOnLocation(location: location, map: mapView, radius: 1000)
    }
    
    func centerMapOnLocation(location: CLLocation, map: MKMapView, radius: CLLocationDistance) {
        let coordinateRegion = MKCoordinateRegionMakeWithDistance(location.coordinate,
                                                                  radius, radius)
        map.setRegion(coordinateRegion, animated: true)
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
    }
}


//
//  Colors.swift
//  SafeReturn
//
//  Created by Jakub Ziembiński on 08/04/2017.
//  Copyright © 2017 Jakub Ziembiński. All rights reserved.
//

import UIKit

extension UIColor {
    
    static let panicBorder = UIColor(red: 0.76, green: 0.10, blue: 0.04, alpha: 1.0)
    static let tagBorder = UIColor(red:0.96, green:0.67, blue:0.21, alpha:1.0)
    static let checkBorder = UIColor(red:0.81, green:0.80, blue:0.82, alpha:1.0)
    
    static let panicEnabled = UIColor(red: 0.76, green: 0.10, blue: 0.04, alpha: 0.75)
    static let tagEnabled = UIColor(red: 0.96, green: 0.67, blue: 0.21, alpha: 0.75)
    
    static let checkEnabled = UIColor(red: 0.81, green: 0.80, blue: 0.82, alpha: 0.75)
    static let checkUsed = UIColor(red: 0.13, green: 0.65, blue: 0.94, alpha: 1.0)
    
    static let background = UIColor(red: 1.0, green: 1.0, blue: 1.0, alpha: 0.25)
    
}

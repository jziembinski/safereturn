//
//  GeneralButton.swift
//  SafeReturn
//
//  Created by Jakub Ziembiński on 08/04/2017.
//  Copyright © 2017 Jakub Ziembiński. All rights reserved.
//

import UIKit

class GeneralButton: UIButton {
    
    required init?(coder aDecoder: NSCoder) {
        
        super.init(coder: aDecoder)
        
        self.clipsToBounds = true
        self.layer.cornerRadius = self.bounds.height/2
        self.layer.borderWidth = 3
        self.backgroundColor = UIColor.background
    }
    
    func highlight(duration: TimeInterval, to alpha: CGFloat) {
        UIButton.animate(withDuration: duration) {
            self.backgroundColor = UIColor(cgColor: self.layer.borderColor!).withAlphaComponent(alpha)
        }
    }
    
    func dim() {
        UIButton.animate(withDuration: 0.1) {
            self.backgroundColor = UIColor.background
        }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesEnded(touches, with: event)
        
    }
    
}

//
//  Location.swift
//  SafeReturn
//
//  Created by Stanisław Paśkowski on 08.04.2017.
//  Copyright © 2017 Jakub Ziembiński. All rights reserved.
//

import Foundation
import MapKit

struct User {
    
    let name: String
    let email: String
    let friends: [User]
    
    init(name: String, email: String) {
        self.email = email
        self.name = name
        self.friends = []
    }
    
    init(name: String, email: String, friends: [User]) {
        self.email = email
        self.name = name
        self.friends = friends
    }
    
}

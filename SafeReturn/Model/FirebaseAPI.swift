//
//  FirebaseAPI.swift
//  SafeReturn
//
//  Created by Stanisław Paśkowski on 08.04.2017.
//  Copyright © 2017 Jakub Ziembiński. All rights reserved.
//

import Foundation
import Firebase

class FirebaseAPI {
    
    let firebaseAuthentication = FIRAuth.auth()
    let firebaseDB: FIRDatabaseReference = FIRDatabase.database().reference()
    var userId: String?
    var firebaseHandle: FIRDatabaseHandle?
    
    
    func loginUserWith(email: String, password: String, success: @escaping () -> (), failure: @escaping (Error?) -> ()) {
        firebaseAuthentication?.signIn(withEmail: email, password: password, completion: { (user, error) in
            guard error == nil else {
                failure(error!)
                return
            }
            
//            if let user = user {
//                self.userId = user.uid  // Unique ID, which you can use to identify the user on the client side
//            }
            
            success()
        })
    }
    
    func registerUser(email: String, password: String, success: @escaping () -> (), failure: @escaping (Error?) -> ()) {
        firebaseAuthentication?.createUser(withEmail: email, password: password, completion: { (user, error) in
            guard error == nil else {
                print(error!)
                return
            }
            
            if let userId = self.userId {
                self.firebaseDB.child("Users/\(userId)").setValue(self.userId);
            }
            
            print("We've got user registed: \(String(describing: user))")
            self.firebaseAuthentication?.signIn(withEmail: email, password: password, completion: { (user, error) in
                guard error == nil else {
                    failure(error)
                    return
                }
                success()
            })
            
        })
    }
}
